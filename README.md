# C-AST serialization to JSON

This tool serializes the AST of a C file as a JSON object.

# Build

**Install clang-9.0 in the system so that `clang-config` is visible in the `PATH`.**

Follow [step 0 of this tutorial](https://clang.llvm.org/docs/LibASTMatchersTutorial.html)
if you need help installing Clang system-wide.

```shell
$ make
```

# Installation

Installing is done by running
```shell
$ sudo make install
```

`DESTDIR` is optional and by default it targets `/usr/local`, you can override
it to install at the user level.

```shell
$ make install DESTDIR=dist/
$ tree dist
dist
├── bin
│   ├── c-to-json
│   └── cu-to-json
└── share
    └── c-to-json
        └── include
            └── cuda.h

4 directories, 3 files
```
