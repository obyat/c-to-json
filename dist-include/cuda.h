#ifndef CUDA_H

#define CUDA_H
extern void __requires(bool);

#define __device__ __attribute__((annotate("device")))
#define __global__ __attribute__((annotate("global")))
#define __shared__ __attribute__((annotate("shared")))
#define __host__ __attribute__((annotate("host")))

typedef unsigned short ushort;
typedef unsigned int uint;

/* See Table B-1 in CUDA Specification */
/* From vector_functions.h */

#define __MAKE_VECTOR_OPERATIONS(TYPE,NAME) \
  typedef struct {   \
    TYPE x;          \
  } NAME##1;         \
  typedef struct {   \
    TYPE x, y;       \
  } NAME##2;         \
  typedef struct {   \
    TYPE x, y, z;    \
  } NAME##3;         \
  typedef struct {   \
    TYPE x, y, z, w; \
  } NAME##4;         \
  __host__ __device__ static __inline__ NAME##1 make_##NAME##1(TYPE x) \
  { \
    return { x }; \
  } \
  __host__ __device__ static __inline__ NAME##2 make_##NAME##2(TYPE x, TYPE y) \
  { \
    return { x, y }; \
  } \
  __host__ __device__ static __inline__ NAME##2 make_##NAME##2(TYPE x) \
  { \
    return make_##NAME##2(x, x); \
  } \
  __host__ __device__ static __inline__ NAME##3 make_##NAME##3(TYPE x, TYPE y, TYPE z) \
  { \
    return { x, y, z }; \
  } \
  __host__ __device__ static __inline__ NAME##3 make_##NAME##3(TYPE x) \
  { \
    return make_##NAME##3(x, x, x); \
  } \
  __host__ __device__ static __inline__ NAME##4 make_##NAME##4(TYPE x, TYPE y, TYPE z, TYPE w) \
  { \
    return { x, y, z, w }; \
  } \
  __device__ static __inline__ NAME##4 make_##NAME##4(TYPE x) \
  { \
    return make_##NAME##4(x, x, x, x); \
  }

__MAKE_VECTOR_OPERATIONS(signed char,char)
__MAKE_VECTOR_OPERATIONS(unsigned char,uchar)
__MAKE_VECTOR_OPERATIONS(short, short)
__MAKE_VECTOR_OPERATIONS(unsigned short,ushort)
__MAKE_VECTOR_OPERATIONS(int,int)
__MAKE_VECTOR_OPERATIONS(unsigned int,uint)
__MAKE_VECTOR_OPERATIONS(long,long)
__MAKE_VECTOR_OPERATIONS(unsigned long,ulong)
__MAKE_VECTOR_OPERATIONS(long long,longlong)
__MAKE_VECTOR_OPERATIONS(unsigned long long,ulonglong)
__MAKE_VECTOR_OPERATIONS(float,float)
__MAKE_VECTOR_OPERATIONS(double,double)

#undef __MAKE_VECTOR_OPERATIONS

/* from helper_math.h */

#define __MAKE_VECTOR_FROM_SIMILAR(TYPE,ZERO) \
  __host__ __device__ static __inline__ TYPE##2 make_##TYPE##2(TYPE##3 a) \
  { \
    return make_##TYPE##2(a.x, a.y);  /* discards z */ \
  } \
  __host__ __device__ static __inline__ TYPE##3 make_##TYPE##3(TYPE##2 a) \
  { \
    return make_##TYPE##3(a.x, a.y, ZERO); \
  } \
  __host__ __device__ static __inline__ TYPE##3 make_##TYPE##3(TYPE##2 a, TYPE s) \
  { \
    return make_##TYPE##3(a.x, a.y, s); \
  } \
  __host__ __device__ static __inline__ TYPE##4 make_##TYPE##4(TYPE##3 a) \
  { \
    return make_##TYPE##4(a.x, a.y, a.z, ZERO); \
  } \
  __host__ __device__ static __inline__ TYPE##4 make_##TYPE##4(TYPE##3 a, TYPE w) \
  { \
    return make_##TYPE##4(a.x, a.y, a.z, w); \
  }

__MAKE_VECTOR_FROM_SIMILAR(float,0.0f)
__MAKE_VECTOR_FROM_SIMILAR(int,0)
__MAKE_VECTOR_FROM_SIMILAR(uint,0)

#undef __MAKE_VECTOR_FROM_SIMILAR



struct __attribute__((device_builtin)) dim3
{
    unsigned int x, y, z;
};
typedef __attribute__((device_builtin)) struct dim3 dim3;

struct __attribute__((device_builtin)) dim4
{
    unsigned int x, y, z, w;
};
typedef __attribute__((device_builtin)) struct dim4 dim4;

uint3 __attribute__((device_builtin)) extern const threadIdx;
uint3 __attribute__((device_builtin)) extern const blockIdx;
dim3 __attribute__((device_builtin)) extern const blockDim;
dim3 __attribute__((device_builtin)) extern const gridDim;
int __attribute__((device_builtin)) extern const warpSize;

extern __attribute__((annotate("device"))) __attribute__((device_builtin)) void __syncthreads(void);
extern __device__ long long atomicAdd(long long* address, int val);
extern __device__ unsigned long long atomicAdd(unsigned long long* address, int val);

#endif /* CUDA_H */
